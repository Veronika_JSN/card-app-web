import './App.css';
import {Layout} from './page';
import {BrowserRouter} from 'react-router-dom';
import {ThemeProvider} from '@material-ui/core';
import {customTheme} from './theme/customTheme';

export const App = () => {

    return (
            <ThemeProvider theme={customTheme}>
                <BrowserRouter>
                    <Layout/>
                </BrowserRouter>
            </ThemeProvider>
    );
}