import {TextField} from "@material-ui/core";

export const Input = ({name, label, type, placeholder, ...other}) => {

    return (
        <TextField
            variant='filled'
            fullWidth
            name={name}
            label={label}
            type={type}
            placeholder={placeholder}
            {...other.inputRef}
        />
    )
}
