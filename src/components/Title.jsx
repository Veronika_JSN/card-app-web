import {Typography} from "@material-ui/core";

export const Title = ({children}) => {

    return (
        <Typography
            variant="h6"
            align="left"
        >
            {children}
        </Typography>
    )
}

