export const PASSWORD_MIN_LENGTH = 6;

export const SIGN_UP_FIELDS = [
    {
        id: 'nickname',
        label: 'Nickname',
        type: 'text',
        name: 'nickname',
        validationRuled: {
            required: 'Nickname is required'
        }
    },
    {
        id: 'password',
        label: 'Password',
        type: 'password',
        name: 'password',
        validationRuled: {
            required: 'Password is required',
            minLength: {
                value: PASSWORD_MIN_LENGTH,
                message: `Password should be at least ${PASSWORD_MIN_LENGTH} characters long`
            }
        }
    },
    {
        id: 'confirmPassword',
        label: 'Confirm password',
        type: 'password',
        name: 'confPass',
        validationRuled: {
            required: 'Confirm password is required'
        }
    }
];

export const SIGN_IN_FIELDS = [
    {
        id: 'nickname',
        label: 'Nickname',
        type: 'text',
        name: 'nickname',
        validationRuled: {
            required: 'Nickname is required'
        }
    },
    {
        id: 'password',
        label: 'Password',
        type: 'password',
        name: 'password',
        validationRuled: {
            required: 'Password is required',
            minLength: {
                value: PASSWORD_MIN_LENGTH,
                message: `Password should be at least ${PASSWORD_MIN_LENGTH} characters long`
            }
        }
    },
];