export const USERS_QUANTITY = 15;

export const columns = [
    {
        id: 1,
        field: 'id',
        headerName: 'ID',
        width: 90
    },
    {
        id: 3,
        field: 'nickname',
        headerName: 'Nickname',
        width: 150,
    },
    {
        id: 4,
        field: 'role',
        headerName: 'Role',
        width: 150,
    },
    {
        id: 5,
        field: 'actions',
        headerName: 'Actions',
        width: 100,
    },
];

export const ADMIN_ROLE = 'admin';
export const USER_ROLE = 'user';