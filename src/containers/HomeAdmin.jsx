import {useEffect} from 'react';
import {Home} from '../templates';
import {useSelector, useDispatch} from 'react-redux';
import {loadBatchCards} from '../store';

export const HomeAdminContainer = () => {

    const dispatch = useDispatch();

    useEffect(() => {

        dispatch(loadBatchCards(1))

    }, [dispatch]);

    const {cards, quantity} = useSelector(state => state.card);

    const handleLoadPage = (event, value) => {
        dispatch(loadBatchCards(value))
    }

    return (
        <Home cards={cards} quantity={quantity} onLoadPage={handleLoadPage}/>
    );
};