import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {infoUser, loadingUser} from "../store";

export const useAuthenticateUser = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        const accessToken = localStorage.getItem('accessToken');

        if (accessToken === null) {
            dispatch(loadingUser())
            return
        }

        dispatch(infoUser());

    }, [dispatch]);
};