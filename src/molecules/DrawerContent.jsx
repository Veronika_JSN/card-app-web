import {Divider, List, ListItem, ListItemIcon, ListItemText, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {useSelector} from "react-redux";
import * as variables from "../theme/variables";
import TelegramIcon from '@material-ui/icons/Telegram';
import EditIcon from '@material-ui/icons/Edit';
import SpellcheckIcon from '@material-ui/icons/Spellcheck';

const DRAWER_FIELDS = [
    {
        id: 1,
        text: 'Card edit',
        to: '/',
        icon: <EditIcon />,
    },
    {
        id: 2,
        text: 'Users',
        to: '/users',
        icon: <SpellcheckIcon />,
    },
    {
        id: 3,
        text: 'Chat',
        to: '/chat',
        icon: <TelegramIcon />
    }
]

export const DrawerContent = () => {

    const classes = useStyles();
    const name = useSelector(state => state.auth.user.userData.user.nickname);

    return (
        <div>
            <div className={classes.toolbar}/>
            <Typography className={classes.shadow}>Hello {name}</Typography>
            <Divider/>
            <List>
                {DRAWER_FIELDS.map(({id, text, to, icon}) => (
                    <ListItem button component={Link} key={id} to={to}>
                        <ListItemIcon> {icon} </ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    )
}

const useStyles = makeStyles(theme => ({

    toolbar: theme.mixins.toolbar,

    shadow: {
        fontSize: '24px',
        textAlign: "center",
        color: 'primary',
        textShadow: `2px 0px 2px ${variables.primaryClr}`
    }

}));