import {CardContent, CardMedia, Typography} from "@material-ui/core";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {ALIVE_STATUS} from '../constants/card'

export const MyCard = ({card}) => {

    const classes = useStyles();

    return (
        <div className={classes.details}>
            <CardMedia
                className={classes.cover}
                image={card.image}
                title={card.name}
            />
            <CardContent className={classes.content}>
                <Typography gutterBottom variant='h6'>
                    {card.name}
                </Typography>
                <div className={classes.flex}>
                    <p className={card.status === ALIVE_STATUS ? classes.greenCircle : classes.redCircle}/>
                    <p>{card.status} - {card.species}</p>
                </div>
                <p className={classes.label}>Last known location:</p>
                <p>{card.locations ? card.locations.name : 'unknown'}</p>
                <p className={classes.label}>First seen in:</p>
                <p>{card.episodes[0].name}</p>
            </CardContent>
        </div>
    )
}

export const useStyles = makeStyles({

    details: {
        display: 'flex',
        backgroundColor: '#3c3e44',
        color: '#f5f5f5',
        borderRadius: 16,
        height: '230px',
    },
    content: {
        flex: '1 0 auto'
    },
    cover: {
        width: '200px',
        borderRadius: '16px 0 0 16px',
    },
    wrapper: {
        maxWidth: '100%',
    },
    redCircle: {
        background: '#c70000',
        width: '10px',
        height: '10px',
        borderRadius: '100%',
        margin: '18px 8px 0 0'
    },
    greenCircle: {
        background: '#0b8a00',
        width: '10px',
        height: '10px',
        borderRadius: '100%',
        margin: '18px 8px 0 0'
    },
    flex: {
        display: 'flex'
    },
    label: {
        color: '#c2c2c2',
        marginBottom: -10
    }

});