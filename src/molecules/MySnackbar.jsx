import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import {useDispatch, useSelector} from 'react-redux';
import {Alert} from './Alert';
import {catchError} from "../store";

export const MySnackbar = () => {

    const classes = useStyles();
    const text = useSelector(state => state.error.errorMessage)

    const dispatch = useDispatch();

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        dispatch(catchError(null));
    };

    return (
        <div className={classes.root}>

            <Snackbar open={!!text} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {text}
                </Alert>
            </Snackbar>

        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));