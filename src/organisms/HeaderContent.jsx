import {Button, IconButton, Toolbar, Typography} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import images from "../resources/1.png";
import {Link, useHistory} from "react-router-dom";
import {useSelector, useDispatch} from 'react-redux';
import { logoutUser } from '../store';
import MenuIcon from "@material-ui/icons/Menu";

const authSelector = state => !!state.auth.user;


export const HeaderContent = ({onDrawerToggle}) => {

    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();

    const handleRedirectToHomePage = () => {
        history.push('/');
    };

    const handleLogout = () => {
        dispatch(logoutUser());
        history.push('/auth');
    };

    const isAuthentication = useSelector(authSelector);

    return (
        <Toolbar>
            <IconButton edge="start" onClick={handleRedirectToHomePage}>
                <img
                    src={images}
                    alt="Rick"
                    className={classes.logo}
                />
            </IconButton>

            <IconButton
                color="inherit"
                aria-label="open drawerContent"
                edge="start"
                onClick={onDrawerToggle}
                className={classes.barButton}
            >
                <MenuIcon />
            </IconButton>

            <Typography
                variant="h6"
                align="left"
                className={classes.title}
            >
                The Rick and Morty cards store
            </Typography>

            {!isAuthentication && (
                <>
                    <Button
                        color="inherit"
                        variant="outlined"
                        component={Link}
                        className={classes.menuButton}
                        to='/signIn'
                    >
                        Log In
                    </Button>
                    <Button variant="contained" component={Link} to='signUp'>Sign Up</Button>
                </>

            )}

            {isAuthentication && (
                <>
                    <Button
                        color="inherit"
                        variant="outlined"
                        component={Link}
                        className={classes.menuButton}
                        to='/signIn'
                        onClick={handleLogout}
                    >
                        Log out
                    </Button>

                </>

            )}

        </Toolbar>
    )
}

export const useStyles = makeStyles(theme => ({

    menuButton: {
        marginRight: theme.spacing(1)
    },

    title: {
        flexGrow: 1
    },

    logo: {
        maxHeight: '40px'
    },

    barButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },

}));