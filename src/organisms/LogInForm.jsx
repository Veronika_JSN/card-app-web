import {Title, Input} from "../components";
import {SIGN_IN_FIELDS} from '../constants/auth';
import {Button, Typography} from "@material-ui/core";
import SendIcon from '@material-ui/icons/Send';
import {makeStyles} from '@material-ui/core/styles';
import {useForm} from 'react-hook-form';
import {ErrorMessage} from '@hookform/error-message';
import {useDispatch} from 'react-redux';
import {loginUser} from '../store';


export const LogInForm = () => {

    const {register, handleSubmit, formState: {errors}} = useForm();
    const classes = useStyles();

    const dispatch = useDispatch();

    const onSubmit = async value => {

        dispatch(loginUser(value));

    };

    return (
        <>
            <Title>Log in to your account</Title>
            <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>{
                SIGN_IN_FIELDS.map(({id, validationRuled, label, type, name}) => (

                    <div key={id} className={classes.inputWrapper}>
                        <Input
                            name={name}
                            label={label}
                            type={type}
                            inputRef={{...register(name, validationRuled)}}
                        />
                        <ErrorMessage
                            errors={errors}
                            name={name}
                            render={({message}) => <Typography className={classes.mistake}>{message}</Typography>}
                        />
                    </div>
                ))
            }
                <Button type='submit' variant="outlined" color="secondary" size='large'
                        endIcon={<SendIcon/>}>Submit</Button>

            </form>
        </>

    )
}

export const useStyles = makeStyles(theme => ({

    mistake: {
        color: 'red',
        marginLeft: theme.spacing(2),
        fontSize: '12px'

    },

    inputWrapper: {
        marginTop: theme.spacing(3),
        margin: 0,
        padding: 0,
        '&:last-of-type': {
            paddingBottom: theme.spacing(3),
        },
    }

}));