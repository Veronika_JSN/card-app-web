import {Drawer, Hidden} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';
import {DRAWER_WIDTH} from '../constants/drawer';
import {DrawerContent} from '../molecules/';


export const MyDrawer = ({window, onDrawerToggle, mobileOpen}) => {

    const classes = useStyles();

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <nav className={classes.drawer} aria-label="mailbox folders">
            <Hidden smUp implementation="css">
                <Drawer
                    container={container}
                    variant="temporary"
                    anchor='left'
                    open={mobileOpen}
                    onClose={onDrawerToggle}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true,
                    }}
                >
                    <DrawerContent />
                </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    <DrawerContent />
                </Drawer>
            </Hidden>
        </nav>

    );
}

const useStyles = makeStyles(theme => ({

    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: DRAWER_WIDTH,
            flexShrink: 0,
        },
    },

    drawerPaper: {
        width: DRAWER_WIDTH,
    },

}));