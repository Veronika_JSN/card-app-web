import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {InputBase, IconButton, Paper} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';


export const Search = ({onChangeText, text, onSendRequest, onKeyDown}) => {
    const classes = useStyles();

    return (
        <Paper component="form" className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder="Search by nickname"
                type="text"
                value={text}
                autoComplete='off'
                onChange={onChangeText}
                onKeyDown={onKeyDown}
            />
            <IconButton className={classes.iconButton} onClick={onSendRequest}>
                <SearchIcon />
            </IconButton>
        </Paper>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        margin: '0 auto 20px',
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 500,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
}));