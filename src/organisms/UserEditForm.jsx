import {
    Button,
    Typography,
    FormControlLabel,
    Checkbox, LinearProgress,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useParams} from "react-router-dom";
import {Input, Title} from "../components";
import {ErrorMessage} from "@hookform/error-message";
import SendIcon from "@material-ui/icons/Send";
import {useForm, Controller} from "react-hook-form";
import {SIGN_IN_FIELDS} from "../constants/auth";
import {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import {updateUser, userIsChanged} from "../store";
import {useDispatch, useSelector} from 'react-redux';


const errorSelector = state => !!state.error.errorMessage;

export const UserEditForm = () => {

    const classes = useStyles();

    const [checked, setChecked] = useState(true);
    const [loader, setLoader] = useState(false);

    const history = useHistory();
    const dispatch = useDispatch();
    const isUserCorrectEdit = useSelector(state => state.user.isReady);
    const isError = useSelector(errorSelector);

    const {userId, userNickname} = useParams();

    const {register, handleSubmit, formState: {errors}, control} = useForm();

    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    const onSubmit = async (value) => {
        dispatch(updateUser(
            {
                value: value,
                id: userId
            })
        );
        setLoader(true)
    };

    useEffect(() => {
        return () => {
            dispatch(userIsChanged());
            setLoader(false)
        }
    }, [])

    useEffect(() => {
        setLoader(false)
    }, [isError])


    if (isUserCorrectEdit) {
        history.push('/users');
    }

    return (
        <>
            <Title>Edit user {userNickname}</Title>
            <form autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
                {
                    SIGN_IN_FIELDS.map(({id, validationRuled, label, type, name}) => (
                        <div key={id} className={classes.inputWrapper}>
                            <Input
                                name={name}
                                label={label}
                                type={type}
                                inputRef={{...register(name, validationRuled)}}
                            />
                            <ErrorMessage
                                errors={errors}
                                name={name}
                                render={({message}) => <Typography
                                    className={classes.mistake}>{message}</Typography>}
                            />
                        </div>
                    ))
                }

                <FormControlLabel
                    className={classes.checked}
                    label={"User is admin"}
                    control={
                        <Controller
                            name="isAdmin"
                            control={control}
                            defaultValue={true}
                            value={checked}
                            render={({field}) => <Checkbox {...field} defaultChecked/>}
                            onClick={handleChange}
                        />
                    }
                />
                {
                    loader &&
                    <LinearProgress/>
                }
                <Button type='submit' variant="outlined" color="secondary" size='large' className={classes.button}
                        endIcon={<SendIcon/>}>Submit</Button>

            </form>
        </>

    )
}

export const useStyles = makeStyles(theme => ({

    button: {
        marginTop: '10px'
    },

    mistake: {
        color: 'red',
        marginLeft: theme.spacing(2),
        fontSize: '12px'
    },

    inputWrapper: {
        marginTop: theme.spacing(3),
        maxWidth: '600px',
        margin: 0,
        padding: 0,
        '&:last-of-type': {
            paddingBottom: theme.spacing(3),
        },
    },

    checked: {
        display: 'block',
        paddingBottom: theme.spacing(3)
    }

}));