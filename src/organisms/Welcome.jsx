import images from "../resources/3.png";
import {makeStyles} from "@material-ui/core/styles";
import {Link} from "react-router-dom";

export const Welcome = () => {

    const classes = useStyles();

    return (
        <>
            <p>
                Crazy about these two. I want all the cards with them. You
                are one of us? <Link to="/signUp">Sing up</Link>
            </p>
            <img
                src={images}
                alt="a couple of freaks"
                className={classes.image}
            />
        </>
    )
}

export const useStyles = makeStyles({

    image: {
        maxHeight: '70vh'
    }

});