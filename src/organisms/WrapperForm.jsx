import {Container, Paper} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';

export const WrapperForm = ({form}) => {

    const classes = useStyles();

    return (
        <Container fixed>
            <Paper className={classes.main}>
                <div className={classes.wrapper}>
                    {form}
                </div>
            </Paper>
        </Container>
    )
}

export const useStyles = makeStyles(theme => ({

    main: {
        padding: theme.spacing(20),
        alignItems: 'center',
        minHeight: '75vh',
    },

    wrapper: {
        maxWidth: '600px',
        margin: '0 auto'
    },

}));