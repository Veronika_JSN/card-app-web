import {Route, Switch, Redirect} from "react-router-dom";
import {WelcomeContent, SignUp, Header, LogIn, Chat, Error, Users, UserEdit} from "../templates";
import {useAuthenticateUser} from '../hooks';
import {useSelector} from 'react-redux';
import {MySnackbar} from "../molecules";
import {HomeAdminContainer} from '../containers';
import CssBaseline from "@material-ui/core/CssBaseline";
import {makeStyles} from "@material-ui/core/styles";
import {useState} from "react";
import {MyDrawer} from "../organisms";
import {PrivateRoute} from '../route';

const authSelector = state => state.auth;
const errorSelector = state => !!state.error.errorMessage;


export const Layout = () => {

    useAuthenticateUser();

    const classes = useStyles();

    const [mobileOpen, setMobileOpen] = useState(false);
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const authData = useSelector(authSelector);
    const isError = useSelector(errorSelector);

    const isShowLoader = authData.loading;

    if (isShowLoader) return <span>Loading...</span>;

    const isAuthenticated = !!authData.user;

    const isAdmin = isAuthenticated ? (!!authData.user.userData.user.is_admin) : false;

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <Header onDrawerToggle={handleDrawerToggle} open={mobileOpen}/>

            {isAuthenticated && (<MyDrawer onDrawerToggle={handleDrawerToggle} mobileOpen={mobileOpen}/>)}

            <main className={classes.content}>

                <Switch>

                    <Route path='/error403' exact>
                        <Error/>
                    </Route>

                    {!isAuthenticated &&
                    <Route path='/' exact>
                        <WelcomeContent/>
                    </Route>
                    }

                    <PrivateRoute path='/signUp' redirectTo='/' isAuthenticated={!isAuthenticated}>
                        <SignUp/>
                    </PrivateRoute>

                    <PrivateRoute path='/signIn' redirectTo='/' isAuthenticated={!isAuthenticated}>
                        <LogIn/>
                    </PrivateRoute>

                    <PrivateRoute path='/users' exect redirectTo='/error403' isAuthenticated={isAdmin}>
                        <Users/>
                    </PrivateRoute>

                    <PrivateRoute path='/chat' exect redirectTo='/error403' isAuthenticated={isAuthenticated}>
                        <Chat/>
                    </PrivateRoute>

                    <PrivateRoute path='/editUser/:userId-:userNickname' exect redirectTo='/error403' isAuthenticated={isAdmin}>
                        <UserEdit/>
                    </PrivateRoute>

                    {isAuthenticated &&
                    <PrivateRoute path='/' exect redirectTo='/error403' isAuthenticated={isAuthenticated}>
                        <HomeAdminContainer/>
                    </PrivateRoute>
                    }

                    <Redirect to="/"/>

                </Switch>
                {isError && <MySnackbar/>}
            </main>
        </div>
    );
};

export const useStyles = makeStyles(theme => ({

    root: {
        display: 'flex',
    },

    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },

}));