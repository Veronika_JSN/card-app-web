import {Route, Redirect} from "react-router-dom";

export const PrivateRoute = ({ children, redirectTo, isAuthenticated, ...rest }) => {

    return (
        <Route
            {...rest}
            render={() =>
                isAuthenticated ? (
                    children
                ) : (
                    <Redirect
                        to={redirectTo}
                    />
                )
            }
        />
    );
}