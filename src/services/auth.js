import {axiosInstance} from './axios';

const getData = async () => {

    const accessToken = localStorage.getItem('accessToken');
    const config = {
        headers: {
            Authorization: "Bearer " + accessToken
        }
    }
    const {data} = await axiosInstance.get('/users/my', config);
    return data
}

export async function fetchMyInfoUser() {

    try {

        const response = await getData();
        return response

    } catch (e) {

        const ownRefreshToken = localStorage.getItem('refreshToken');
        const {
            data: {
                accessToken,
                refreshToken
            }
        } = await axiosInstance.post('/users/refresh', {token: ownRefreshToken});

        window.localStorage.setItem('accessToken', accessToken);
        window.localStorage.setItem('refreshToken', refreshToken);

        return getData();
    }
}


export async function fetchSignupUser(user) {

    const {
        data: {accessToken, refreshToken}
    } = await axiosInstance.post('/users/signup', user);

    window.localStorage.setItem('accessToken', accessToken);
    window.localStorage.setItem('refreshToken', refreshToken);

    const response = await fetchMyInfoUser();
    return response
}

export async function fetchLoginUser(user) {

    const {
        data: {accessToken, refreshToken}
    } = await axiosInstance.post('/users/login', user);

    window.localStorage.setItem('accessToken', accessToken);
    window.localStorage.setItem('refreshToken', refreshToken);

    const response = await fetchMyInfoUser();
    return response
}