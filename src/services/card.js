import {axiosInstance} from './axios';
import {CARD_QUANTITY} from '../constants/card';

export async function fetchCard(amount) {

    const { data } = await axiosInstance.get(`cards/?page=${amount}&pageSize=${CARD_QUANTITY}`);

    return data;

}