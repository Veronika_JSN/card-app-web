import {axiosInstance} from './axios';
import {USERS_QUANTITY} from '../constants/users';

export async function fetchUsers(amount) {

    const { data } = await axiosInstance.get(`users/?page=${amount}&pageSize=${USERS_QUANTITY}`);

    return data;

}


export async function fetchUserByName(text) {

    const { data } = await axiosInstance.get(`users/nickname/?nickname=${text}`);

    return data;

}


const getData = async ({value, id}) => {

    const accessToken = localStorage.getItem('accessToken');

    const config = {
        method: 'put',
        headers: {
            Authorization: "Bearer " + accessToken
        },
        data: value
    }

    const {data} = await axiosInstance(`/users/${id}`, config);
    return data
}


export async function updateUserInfo(data) {

    try {

        const response = await getData(data);
        return response

    } catch (e) {

        const ownRefreshToken = localStorage.getItem('refreshToken');
        const {
            data: {
                accessToken,
                refreshToken
            }
        } = await axiosInstance.post('/users/refresh', {token: ownRefreshToken});

        window.localStorage.setItem('accessToken', accessToken);
        window.localStorage.setItem('refreshToken', refreshToken);

        return getData(data);
    }
}