import * as types from '../types';

export const authenticateUser = (userData) => {

    return {
        type: types.AUTHENTICATE_USER,
        payload: {
            userData
        }
    };
};

export const logoutUser = () => {
    window.localStorage.removeItem('accessToken');
    window.localStorage.removeItem('refreshToken');

    return {
        type: types.LOGOUT_USER
    };
};

export const infoUser = () => {

    return {
        type: types.LOAD_INFO_USER,
    }
};

export const loadingUser = () => {

    return {
        type: types.STOP_LOADING_USER,
    }
};

export const signUpUser = (user) => {

    return {
        type: types.LOAD_SIGN_UP_USER,
        payload: user
    }
};

export const loginUser = (user) => {

    return {
        type: types.LOAD_LOGIN_USER,
        payload: user
    }
};