import * as types from '../types';

export const getBatchCards = ({cardsList, quantity}) => {

    return {
        type: types.PUT_BATCH_CARDS,
        payload: {
            cards: cardsList,
            quantity
        }
    };
};

export const loadBatchCards = (page) => {

    return {
        type: types.LOAD_BATCH_CARDS,
        payload: {
            page
        }
    };
};