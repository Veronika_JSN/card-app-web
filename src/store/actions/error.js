import * as types from "../types";

export const catchError = (message) => {

    return {
        type: types.CATCH_ERROR,
        payload: {
            message
        }
    };
};