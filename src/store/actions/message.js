import * as types from '../types';

export const putMessage = ({ text, created_at, id, userName }) => {

    return {
        type: types.PUT_MESSAGE,
        payload: {
            text,
            created_at,
            id,
            userName
        }
    };
};
