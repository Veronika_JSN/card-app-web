import * as types from '../types';

export const getBatchUsers = ({usersList, quantity}) => {

    return {
        type: types.PUT_BATCH_USERS,
        payload: {
            users: usersList,
            quantity
        }
    };
};

export const loadBatchUsers = (page) => {

    return {
        type: types.LOAD_BATCH_USERS,
        payload: {
            page
        }
    };
};

export const loadUserByName = (text) => {

    return {
        type: types.LOAD_USER_BY_NAME,
        payload: {
            text
        }
    };
};

export const updateUser = ({value, id}) => {

    return {
        type: types.UPDATE_USER,
        payload: {
            value,
            id
        }
    };
};

export const putEditUser = (data) => {

    return {
        type: types.PUT_EDIT_USER,
        payload: {
            data
        }
    };
};

export const userIsChanged = () => {

    return {
        type: types.USER_IS_CHANGED,
    };
};