import createSagaMiddleware from "redux-saga";
import {applyMiddleware, compose, createStore} from "redux";
import {rootReducer} from "./root";
import {rootSaga} from './sagas/rootSagas';
export { rootReducer } from './root';
export * from './actions';

const saga = createSagaMiddleware();
export const store = createStore(rootReducer, compose(applyMiddleware(saga),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

saga.run(rootSaga);