import * as types from '../types';

const initialState = {
    user: null,
    loading: true
};

export const authReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {
        case types.STOP_LOADING_USER: {
            return {
                ...state,
                loading: false
            }
        }

        case types.AUTHENTICATE_USER: {
            return {
                ...state,
                user: payload,
                loading: false
            };
        }

        case types.LOGOUT_USER: {
            return {
                ...state,
                user: null,
            };
        }

        default:
            return state
    }
}