import * as types from '../types';

const initialState = {
    loader: true,
    cards: [],
    quantity: null
};

export const cardReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type){

        case types.PUT_BATCH_CARDS: {
            return {
                loader: false,
                cards: payload.cards,
                quantity: payload.quantity
            };
        }

        default:
            return state
    }
}