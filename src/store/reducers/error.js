import * as types from '../types';

const initialState = {
    errorMessage: null
};

export const errorReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type){

        case types.CATCH_ERROR: {
            return {
                errorMessage: payload.message,
            };
        }

        default:
            return state
    }
}