import * as types from '../types';

const initialState = {
    messageList: []
};

export const messageReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type){

        case types.PUT_MESSAGE: {

            const { messageList } = state;
            messageList.push(payload);
            return {
                messageList: messageList
            };
        }

        default:
            return state
    }
}