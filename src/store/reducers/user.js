import * as types from '../types';

const initialState = {
    users: [],
    quantity: null,
    isReady: false
};

export const userReducer = (state = initialState, action) => {

    const {type, payload} = action;

    switch (type){

        case types.PUT_BATCH_USERS: {
            return {
                ...state,
                users: payload.users,
                quantity: payload.quantity,
            };
        }

        case types.PUT_EDIT_USER: {

            const {users, quantity} = state;
            const {data} = payload;

            const updateUsers = users.map(item=>{
                if (item.id === data.id){
                    return data
                }else{
                    return item
                }
            })

            return {
                ...state,
                users: updateUsers,
                quantity: quantity,
                isReady: true,
            };
        }

        case types.USER_IS_CHANGED: {
            return {
                ...state,
                isReady: false,
            };
        }

        default:
            return state
    }
}