import { combineReducers } from 'redux';
import { authReducer, errorReducer, cardReducer, messageReducer, userReducer } from './reducers';

export const rootReducer = combineReducers({
    auth: authReducer,
    error: errorReducer,
    card: cardReducer,
    message: messageReducer,
    user: userReducer
});