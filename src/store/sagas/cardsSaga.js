import {takeEvery, put, call} from 'redux-saga/effects';
import {catchError, getBatchCards} from "../actions";
import * as types from '../types';
import {fetchCard} from '../../services/card';


function* workerLoadCards({payload: {page}}) {

    try {

        const response = yield call(fetchCard, page);
        yield put(getBatchCards(response));

    } catch (error) {

        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }

}

export function* watchLoadCards() {

    yield takeEvery(types.LOAD_BATCH_CARDS, workerLoadCards);

}