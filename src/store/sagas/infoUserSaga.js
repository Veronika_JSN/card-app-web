import {takeEvery, put, call} from 'redux-saga/effects';
import {authenticateUser, catchError} from "../actions";
import * as types from '../types';
import {fetchMyInfoUser} from '../../services/auth';


function* workerLoadUserInfo() {

    try {
        const userData = yield call(fetchMyInfoUser);
        yield put(authenticateUser(userData));

    } catch (error) {
        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchInfoUser() {

    yield takeEvery(types.LOAD_INFO_USER, workerLoadUserInfo);

}