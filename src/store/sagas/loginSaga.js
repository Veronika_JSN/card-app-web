import {takeEvery, put, call} from 'redux-saga/effects';
import {authenticateUser, catchError} from "../actions";
import * as types from '../types';
import {fetchLoginUser} from '../../services/auth';



function* workerLoadUserData({payload}) {

    try {
        const userData = yield call(fetchLoginUser, payload);
        yield put(authenticateUser(userData));

    } catch (error) {
        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchLoginUser() {

    yield takeEvery(types.LOAD_LOGIN_USER, workerLoadUserData);

}