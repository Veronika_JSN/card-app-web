import {fork, all} from 'redux-saga/effects';
import {watchLoadCards} from './cardsSaga';
import {watchInfoUser} from './infoUserSaga';
import {watchSignUpUser} from './signupSaga';
import {watchLoginUser} from './loginSaga';
import {watchLoadUsers} from './usersSaga';
import {watchLoadUserByName} from './selectUserSaga';
import {watchUpdateUserSaga} from './updateUserSaga';

export function* rootSaga() {
    yield all([
        fork(watchLoadCards),
        fork(watchInfoUser),
        fork(watchSignUpUser),
        fork(watchLoginUser),
        fork(watchLoadUsers),
        fork(watchLoadUserByName),
        fork(watchUpdateUserSaga),
    ])
}