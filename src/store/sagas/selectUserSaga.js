import {call, put, takeEvery} from "redux-saga/effects";
import {fetchUserByName} from "../../services/users";
import {catchError, getBatchUsers} from "../actions";
import * as types from "../types";

function* workerLoadUserByName({payload: {text}}) {

    try {

        const response = yield call(fetchUserByName, text);

        yield put(getBatchUsers({
            usersList: response,
            quantity: response.length
        }));

    } catch (error) {

        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchLoadUserByName() {
    yield takeEvery(types.LOAD_USER_BY_NAME, workerLoadUserByName);
}