import {takeEvery, put, call} from 'redux-saga/effects';
import {authenticateUser, catchError} from "../actions";
import * as types from '../types';
import {fetchSignupUser} from '../../services/auth';


function* workerLoadUserData({payload}) {

    try {
        const userData = yield call(fetchSignupUser, payload);
        yield put(authenticateUser(userData));

    } catch (error) {
        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchSignUpUser() {

    yield takeEvery(types.LOAD_SIGN_UP_USER, workerLoadUserData);

}