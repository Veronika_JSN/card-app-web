import {takeEvery, put, call} from 'redux-saga/effects';
import {catchError, putEditUser} from "../actions";
import * as types from '../types';
import {updateUserInfo} from "../../services/users";


function* workerUpdateUser({payload}) {

    try {
        const data = yield call(updateUserInfo, payload);
        yield put(putEditUser(data));

    } catch (error) {

        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchUpdateUserSaga() {
    yield takeEvery(types.UPDATE_USER, workerUpdateUser);
}