import {takeEvery, put, call} from 'redux-saga/effects';
import {catchError, getBatchUsers} from "../actions";
import * as types from '../types';
import {fetchUsers} from '../../services/users';


function* workerLoadUsers({payload: {page}}) {

    try {

        const response = yield call(fetchUsers, page);
        yield put(getBatchUsers(response));

    } catch (error) {

        if (error.response) {
            yield put(catchError(error.response.data.message));
        } else {
            yield put(catchError('Something went wrong, try again later'));
        }
    }
}

export function* watchLoadUsers() {
    yield takeEvery(types.LOAD_BATCH_USERS, workerLoadUsers);
}