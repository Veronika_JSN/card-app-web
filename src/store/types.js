export const AUTHENTICATE_USER = '[auth] AUTHENTICATE';
export const LOAD_INFO_USER = '[auth] LOADING_INFO';
export const LOGOUT_USER = '[auth] LOGOUT';
export const STOP_LOADING_USER = '[auth] STOP_LOADING';
export const LOAD_SIGN_UP_USER = '[auth] LOAD_SIGN_UP_DATA';
export const LOAD_LOGIN_USER = '[auth] LOAD_LOGIN_DATA';

export const CATCH_ERROR = '[error] CATCH_ERROR';

export const PUT_BATCH_CARDS = '[card] PUT_BATCH';
export const LOAD_BATCH_CARDS = '[card] LOAD_BATCH';

export const PUT_MESSAGE = '[message] PUT';

export const PUT_BATCH_USERS = '[user] PUT_BATCH';
export const LOAD_BATCH_USERS = '[user] LOAD_BATCH';
export const LOAD_USER_BY_NAME = '[user] LOAD_BY_NAME';
export const UPDATE_USER = '[user] UPDATE';
export const PUT_EDIT_USER = '[user] PUT_EDIT';
export const USER_IS_CHANGED = '[user] CHANGED';