import {Container, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import React from "react";
import Pagination from '@material-ui/lab/Pagination';
import {MyCard} from '../../molecules';
import {CARD_QUANTITY} from '../../constants/card'

export const Home = ({cards, quantity, onLoadPage}) => {

    const classes = useStyles();

    return (
        <>
            <div className={classes.toolbar}/>
            <Container className={classes.container}>
                <Grid container spacing={3}>
                    {cards.map((card) => {
                        return (
                            <Grid item key={card.id} xs={12} md={6} lg={4}>

                                <MyCard card={card}/>

                            </Grid>
                        )
                    })}
                </Grid>
            </Container>

            <div className={classes.paginationWrapper}>
                <Pagination count={Math.ceil(quantity / CARD_QUANTITY)} shape="rounded"
                            onChange={onLoadPage}/>
            </div>
        </>
    )
}

export const useStyles = makeStyles(theme => ({

    toolbar: theme.mixins.toolbar,

    container: {
        padding: 0,
        margin: 0,
        maxWidth: "100%"
    },

    paginationWrapper: {
        marginLeft: '30%',
        padding: '40px 0',
    },

}));