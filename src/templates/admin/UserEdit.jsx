import {WrapperForm, UserEditForm} from "../../organisms";

export const UserEdit = () => {

    return (
        <WrapperForm form={<UserEditForm/>} />
    )
}