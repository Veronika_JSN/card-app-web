import {useEffect, useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {loadBatchUsers, loadUserByName} from "../../store";
import {Container, TableCell} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import {Table, TableContainer, TableHead, TableRow, TableBody, Paper, LinearProgress} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import {Search} from "../../organisms";
import {USERS_QUANTITY, columns, ADMIN_ROLE, USER_ROLE} from "../../constants/users";
import {Link} from "react-router-dom";


export const Users = () => {

    const classes = useStyles();

    const [text, setText] = useState('');

    const {users, quantity} = useSelector(state => state.user);

    const dispatch = useDispatch();

    const handleChangeText = event => {
        setText(event.target.value)
    };

    const handleKeyDown = event => {
        if (event.key === 'Enter') {
            event.preventDefault()
            handleSendRequest();
        }
    };

    const handleSendRequest = () => {
        dispatch(loadUserByName(text))
    }

    useEffect(() => {

        dispatch(loadBatchUsers(1))

    }, [dispatch]);

    const handleChange = (event, value) => {
        dispatch(loadBatchUsers(value))
    }

    return (
        <>
            <div className={classes.toolbar}/>
            <Container className={classes.container}>

                <Search onChangeText={handleChangeText} onKeyDown={handleKeyDown} text={text}
                        onSendRequest={handleSendRequest}/>

                {
                    !!!users.length && (
                        <div className={classes.circle}>
                          <LinearProgress />
                        </div>
                    )
                }

                <TableContainer component={Paper} className={classes.table}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                {
                                    columns.map(item => (
                                            <TableCell className={classes.headColor} align='center'
                                                       key={item.id}>{item.headerName}</TableCell>
                                        )
                                    )
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map((row) => (
                                <TableRow key={row.id} className={classes.rowColor}>
                                    <TableCell className={classes.row}>{row.id}</TableCell>
                                    <TableCell>{row.nickname}</TableCell>
                                    <TableCell>{row.is_admin === 1 ? ADMIN_ROLE : USER_ROLE}</TableCell>
                                    <TableCell>
                                        <Link to={`/editUser/${row.id}-${row.nickname}`} className={classes.edit}>
                                            <EditIcon/>Edit
                                        </Link>

                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <div className={classes.flexSection}>
                        <Pagination className={classes.paginationWrapper}
                                    count={Math.ceil(quantity / USERS_QUANTITY)}
                                    shape="rounded"
                                    onChange={handleChange}
                        />
                    </div>
                </TableContainer>

            </Container>

        </>
    )
}


export const useStyles = makeStyles(theme => ({

    circle: {
        textAlign: 'center',
        paddingBottom: '10px'
    },

    toolbar: theme.mixins.toolbar,

    container: {
        padding: 0,
        margin: 0,
        maxWidth: "100%"
    },

    table: {
        width: 650,
        margin: '0 auto'
    },

    row: {
        lineHeight: 3
    },

    headColor: {
        color: theme.palette.common.white,
        backgroundColor: theme.palette.common.black,
    },

    rowColor: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },

    paginationWrapper: {
        margin: '0 auto',
    },

    flexSection: {
        padding: '40px 0',
        display: 'flex'
    },

    edit: {
        color: theme.palette.common.black
    }

}));