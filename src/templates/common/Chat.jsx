import {Container, Paper, Grid, Divider, TextField, List, ListItem, ListItemText, Fab} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useEffect, useState, useRef} from 'react';
import SendIcon from '@material-ui/icons/Send';
import io from 'socket.io-client';
import {useSelector} from 'react-redux';
import moment from 'moment';
import * as variables from "../../theme/variables";

const {REACT_APP_API_URL} = process.env;


export const Chat = () => {

    const classes = useStyles();
    const [messages, setMessages] = useState([]);
    const [text, setText] = useState('');
    const [socket, setSocket] = useState(null);

    const {id, nickname} = useSelector(state => state.auth.user.userData.user);

    const handleChangeText = e => setText(e.target.value);

    const messagesEndRef = useRef(null);

    useEffect(() => {
        scrollToBottom()
        const messageHandler = message => {
            outputMessage(message)
        }

        const socketInstance = io(REACT_APP_API_URL, {transport: ['websocket'], auth: {user: nickname}})
        setSocket(socketInstance)
        socketInstance.on('message', messageHandler)

        return () => {
            socketInstance.disconnect()
        }

    }, [nickname])


    useEffect(() => {
        scrollToBottom()
    }, [messages])


    const outputMessage = (msg) => {
        setMessages(prevState => {
            return [...prevState, msg]
        })
    }

    const handleKeyDown = e => {
        if (e.key === 'Enter') {
            handleSendMessage();
        }
    };

    const handleSendMessage = () => {
        const msg = {
            writerId: id,
            text: text
        }
        socket.emit('chatMessage', msg);
        setText('');
    }

    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({behavior: "smooth"})
    }

    return (
        <>
            <div className={classes.toolbar}/>
            <Container>

                <div>
                    <Grid container component={Paper} className={classes.chatSection}>
                        <Grid item xs={12}>
                            <List className={classes.messageArea}>
                                {messages.map((item) => {
                                    return (
                                        <ListItem key={item.id}>

                                            {!item.userName ?
                                                <Grid container>
                                                    <Grid item xs={12}>
                                                        <ListItemText align="center" secondary={item.text}/>
                                                    </Grid>
                                                </Grid>

                                                : item.userName === nickname ?
                                                    <Grid container>
                                                        <Grid item xs={12}>
                                                            <ListItemText align="left">
                                                                <span className={classes.myMsg}>{item.text}</span>
                                                            </ListItemText>
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <ListItemText align="left"
                                                                          secondary={`${nickname}, ${moment(item.created_at).format('LT')}`}/>
                                                        </Grid>
                                                    </Grid>

                                                    :
                                                    <Grid container>
                                                        <Grid item xs={12}>
                                                            <ListItemText align="right">
                                                                <span className={classes.comeMsg}>{item.text}</span>
                                                            </ListItemText>
                                                        </Grid>
                                                        <Grid item xs={12}>
                                                            <ListItemText align="right"
                                                                          secondary={`${item.userName}, ${moment(item.created_at).format('LT')}`}/>
                                                        </Grid>
                                                    </Grid>
                                            }
                                        </ListItem>
                                    )
                                })
                                }
                                <div ref={messagesEndRef}/>
                            </List>
                            <Divider/>
                            <Grid container style={{padding: '20px'}}>
                                <Grid item xs={11}>
                                    <TextField
                                        id="outlined-basic-email"
                                        label="Type message"
                                        type="text"
                                        name="text"
                                        value={text}
                                        autoComplete='off'
                                        fullWidth
                                        onChange={handleChangeText}
                                        onKeyDown={handleKeyDown}
                                    />

                                </Grid>
                                <Grid item xs={1} align="right">
                                    <Fab color="primary" aria-label="add"><SendIcon onClick={handleSendMessage}/></Fab>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>

            </Container>
        </>
    )
}


export const useStyles = makeStyles(theme => ({

    toolbar: theme.mixins.toolbar,

    table: {
        minWidth: 650,
    },
    chatSection: {
        width: '100%',
        height: '80vh'
    },
    headBG: {
        backgroundColor: '#e0e0e0'
    },
    borderRight500: {
        borderRight: '1px solid #e0e0e0'
    },
    messageArea: {
        height: '70vh',
        overflowY: 'auto'
    },
    myMsg: {
        backgroundColor: `${variables.primaryClr}`,
        padding: '10px',
        borderRadius: '15px 15px 15px 0'
    },
    comeMsg: {
        backgroundColor: `#dbdbdb`,
        padding: '10px',
        borderRadius: '15px 15px 0 15px '
    }

}));