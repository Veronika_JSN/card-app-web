import images from "../../resources/stop.svg";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";

export const Error = () => {
    const classes = useStyles();
    const history = useHistory();

    const handleRedirectToHomePage = () => {
        history.push('/');
    };

    return (
        <div className={classes.wrapper}>


            <img className={classes.images} src={images} alt="error"/>
            <div>
                <p>403 FORBIDDEN</p>
                <button onClick={handleRedirectToHomePage}>Back to home</button>
            </div>


        </div>
    )
}

export const useStyles = makeStyles({

    wrapper: {
        width: '100%',
        height: '90vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

    },
    images: {
        width: '150px',
        margin: '20px'
    }

});