import {AppBar, Container} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {HeaderContent} from "../../organisms";
import * as variables from '../../theme/variables';
import {DRAWER_WIDTH} from '../../constants/drawer'
import clsx from 'clsx';
import {useSelector} from "react-redux";

const authSelector = state => !!state.auth.user;


export const Header = ({onDrawerToggle, open}) => {

    const isAuthentication = useSelector(authSelector);

    const classes = useStyles();

    return (
        <AppBar position="fixed" className={!isAuthentication ? classes.header : clsx(classes.appBar, {
            [classes.appBarShift]: open,
        })} >
            <Container fixed>
                <HeaderContent onDrawerToggle={onDrawerToggle} />
            </Container>
        </AppBar>
    );
};


export const useStyles = makeStyles(theme => ({

    header: {
        background: `linear-gradient(to right, ${variables.primaryClr}, ${variables.secondaryClr})`,
        color: '#fff',
    },

    appBar: {
        background: `linear-gradient(to right, ${variables.primaryClr}, ${variables.secondaryClr})`,
        color: '#fff',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${DRAWER_WIDTH}px)`,
            marginLeft: DRAWER_WIDTH,
        },
    },

    appBarShift: {
        width: `calc(100% - ${DRAWER_WIDTH}px)`,
        marginLeft: DRAWER_WIDTH,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },

}));