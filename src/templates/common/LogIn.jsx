import {LogInForm, WrapperForm} from "../../organisms";

export const LogIn = () => {

    return (
        <WrapperForm form={<LogInForm/>} />
    )
}