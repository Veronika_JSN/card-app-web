import {SignUpForm, WrapperForm} from "../../organisms";

export const SignUp = () => {

    return (
        <WrapperForm form={<SignUpForm/>} />
    )
}