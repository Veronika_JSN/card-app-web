import {Paper, Container} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {Welcome} from "../../organisms";

export const WelcomeContent = () => {

    const classes = useStyles();

    return (
        <Container fixed>
            <Paper className={classes.main}>
                <Welcome/>
            </Paper>
        </Container>
    );
};

const useStyles = makeStyles(theme => ({

    main: {
        padding: theme.spacing(9),
        display: 'flex',
        alignItems: 'center',
        minHeight: '94vh'
    }

}));

