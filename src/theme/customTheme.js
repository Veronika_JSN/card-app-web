import {createTheme} from "@material-ui/core";
import * as variables from './variables';

export const customTheme = createTheme({
    palette: {
        primary: {
            main: `${variables.primaryClr}`,
        },
        secondary: {
            main: `${variables.secondaryClr}`
        }
    }
});